program BinarySearchTree;

{$APPTYPE CONSOLE}
{$R *.res}

uses
   System.SysUtils,
   BST in 'BST.pas';

var
   BinaryTree: TBST;

function ReadNumber: Integer;
var
   IsCorrect: Boolean;
   Number: Integer;
begin
   IsCorrect := False;
   repeat
      try
         Readln(Number);
         IsCorrect := True;
      except
         Writeln('Try again');
      end;
   until IsCorrect;

   Result := Number;
end;

var
   IsCorrect: Boolean;
   Input: string;
begin

   BinaryTree := TBST.Create;

//   BinaryTree.Add(7);
//   BinaryTree.Add(2);
//   BinaryTree.Add(1);
//   BinaryTree.Add(5);
//   BinaryTree.Add(6);
//   BinaryTree.Add(9);
//   BinaryTree.Add(8);

//   BinaryTree.SymmetricRightThread;
//   BinaryTree.Traverse;

   while True do
   begin

      Writeln('Enter next element of Binary Search Tree');
      Readln(Input);

      if Input = '-' then
      begin
         Writeln('Enter number to DELETE');
         BinaryTree.Delete(ReadNumber);
      end;

      if Input = '+' then
      begin
         Writeln('Enter number to ADD');
         BinaryTree.Add(ReadNumber);
      end;

      if Input = 'traversals' then
         BinaryTree.ThreeTraversals;

      if Input = 'thread' then
      begin
         BinaryTree.SymmetricRightThread;
         BinaryTree.CheckRightTraverse;
      end;

      if Input = 'help' then
      begin
         Writeln('+ : add node');
         Writeln('- : delete node');
         Writeln('traversals : RAB, ARB, ABR');
         Writeln('thread : symmetric right thread');
      end;

      BinaryTree.Traverse;
   end;

   Readln;
end.
