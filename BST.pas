unit BST;

interface

uses
   System.SysUtils, Winapi.Windows;

type
   PNode = ^TNode;

   TNode = record
      Value: Integer;
      Parent, Left, Right: PNode;
      LeftThread, RightThread: PNode;
      Visited: Integer;
   end;

   TBST = class
   private
      Root: PNode;

      procedure UnvisitAll(const Node: PNode);
      function GetParent(const Number: Integer; var StartNode: PNode): PNode;
      function FindParentInBuiltTree(const Number: Integer; var StartNode: PNode): PNode;

      function DFS(const Node: PNode; const Indent: string): PNode;
      procedure RAB(const Node: PNode);
      procedure ARB(const Node: PNode);
      procedure ABR(const Node: PNode);

      procedure RightThread(const Node: PNode);
      procedure TraverseThread(const Node: PNode);
   public
      constructor Create;

      function Add(const Number: Integer): PNode;
      function Delete(const Number: Integer): PNode;

      procedure Traverse;
      procedure ThreeTraversals;
      procedure SymmetricRightThread;
      procedure CheckRightTraverse;
   end;

implementation

var
   Prev: PNode;
   Found: Boolean;

{ TBST }

procedure TBST.ABR(const Node: PNode);
begin
   if Node <> nil then
   begin
      Write(IntToStr(Node.Value) + ' ');

      if Node.Left = nil then
         Write('0 ')
      else
         ABR(Node.Left);

      Write(IntToStr(Node.Value) + ' ');

      if Node.Right = nil then
         Write('0 ')
      else
         ABR(Node.Right);

      SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),
        FOREGROUND_GREEN);
      Write(IntToStr(Node.Value) + ' ');
      SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_RED or
        FOREGROUND_GREEN or FOREGROUND_BLUE);
   end;
end;

function TBST.Add(const Number: Integer): PNode;
var
   Current, Temp, NewNode, Parent: PNode;
begin
   New(NewNode);
   NewNode.Value := Number;
   NewNode.Parent := nil;
   NewNode.Left := nil;
   NewNode.Right := nil;
   NewNode.LeftThread := nil;
   NewNode.RightThread := nil;
   NewNode.Visited := 0;

   if Root = nil then
      Root := NewNode
   else
   begin
      Parent := GetParent(Number, Root);
      NewNode.Parent := Parent;

      if Number < Parent.Value then
         Parent.Left := NewNode
      else if Number > Parent.Value then
         Parent.Right := NewNode;
   end;
end;

function TBST.FindParentInBuiltTree(const Number: Integer;
  var StartNode: PNode): PNode;
begin

   if StartNode.Value = Number then
   begin
      Result := StartNode.Parent;
      Found := True;
   end
   else
   begin
      if (StartNode.Left <> nil) and not Found then
         Result := FindParentInBuiltTree(Number, StartNode.Left);

      if (StartNode.Right <> nil) and not Found then
         Result := FindParentInBuiltTree(Number, StartNode.Right);
   end;

   if not Found then
      Result := nil;
end;

function TBST.Delete(const Number: Integer): PNode;
var
   Parent: PNode;
begin
   if Root = nil then
      Writeln('TREE IS EMPTY, ADD ELEMENT FIRST')
   else
   begin
      if Root.Value = Number then
         Root := nil
      else
      begin
         Found := False;
         Parent := FindParentInBuiltTree(Number, Root);
         if Parent = nil then
            Writeln('NODE NOT FOUND')
         else
         begin
           if Parent.Left <> nil then
            begin
               if Parent.Left.Value = Number then
                  Parent.Left := nil
            end;

            if Parent.Right <> nil then
            begin
               if Parent.Right.Value = Number then
                  Parent.Right := nil;
            end;
         end;
      end;
   end;
end;

procedure TBST.ARB(const Node: PNode);
begin
   if Node <> nil then
   begin
      Write(IntToStr(Node.Value) + ' ');

      if Node.Left = nil then
         Write('0 ')
      else
         ARB(Node.Left);

      SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),
        FOREGROUND_GREEN);
      Write(IntToStr(Node.Value) + ' ');
      SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_RED or
        FOREGROUND_GREEN or FOREGROUND_BLUE);

      if Node.Right = nil then
         Write('0 ')
      else
         ARB(Node.Right);

      Write(IntToStr(Node.Value) + ' ');
   end;
end;

procedure TBST.CheckRightTraverse;
var
   Temp: PNode;
   ShouldGoLeft: Boolean;
begin
   Temp := Root;
   ShouldGoLeft := True;

   Writeln('------------ Symmetric Right Traversal ------------');
   while True do
   begin
      if (Temp = Root) and (Temp.Visited = 1) then
         Break;

      if ShouldGoLeft then
         while Temp.Left <> nil do
            Temp := Temp.Left;


      Write(IntToStr(Temp.Value) + ' ');

      Inc(Temp.Visited);
      if Temp.Right <> nil then
      begin
         ShouldGoLeft := True;
         Temp := Temp.Right;
      end
      else if Temp.RightThread <> nil then
      begin
         ShouldGoLeft := False;
         Temp := Temp.RightThread;
      end;
   end;

   Writeln;
   Writeln('-------------------------------------------------');
   UnvisitAll(Root);
end;

constructor TBST.Create;
begin
   New(Root);

   Root := nil;
end;


function TBST.DFS(const Node: PNode; const Indent: string): PNode;
const
   Tab = '   ';
var
   NewIndent: string;
begin
   NewIndent := Indent + Tab;

   Writeln(Indent + IntToStr(Node.Value));

   if Node.Right = nil then
      if Node.RightThread = nil then
         Writeln(NewIndent + 'empty')
      else
         Writeln(NewIndent + '-> ' + IntToStr(Node.RightThread.Value))
   else
      DFS(Node.Right, NewIndent);

   if Node.Left = nil then
      if Node.LeftThread = nil then
         Writeln(NewIndent + 'empty')
      else
         Writeln(NewIndent + '-> ' + IntToStr(Node.LeftThread.Value))
   else
      DFS(Node.Left, NewIndent);
end;



function TBST.GetParent(const Number: Integer; var StartNode: PNode): PNode;
var
   Current, Temp, NewNode, TargetNode: PNode;
begin
   if StartNode = nil then
      Result := StartNode.Parent
   else
   begin
      Current := StartNode;

      if Number < Current.Value then
      begin
         if Current.Left = nil then
            Result := Current
         else
            Result := GetParent(Number, Current.Left)
      end
      else if Number > Current.Value then
      begin
         if Current.Right = nil then
            Result := Current
         else
            Result := GetParent(Number, Current.Right)
      end;
   end;
end;

procedure TBST.RAB(const Node: PNode);
begin
   if Node <> nil then
   begin
      SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),
        FOREGROUND_GREEN);
      Write(IntToStr(Node.Value) + ' ');
      SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_RED or
        FOREGROUND_GREEN or FOREGROUND_BLUE);

      if Node.Left = nil then
         Write('0 ')
      else
         RAB(Node.Left);

      Write(IntToStr(Node.Value) + ' ');

      if Node.Right = nil then
         Write('0 ')
      else
         RAB(Node.Right);

      Write(IntToStr(Node.Value) + ' ');
   end;
end;

procedure TBST.RightThread(const Node: PNode);
begin
  if Prev <> nil then
      if Prev.Right = nil then
          Prev.RightThread := Node;

  Prev := Node;
end;


procedure TBST.TraverseThread(const Node: PNode);
begin
   if Node <> nil then
   begin
      TraverseThread(Node.Left);
      RightThread(Node);
      TraverseThread(Node.Right);
   end;
end;

procedure TBST.UnvisitAll(const Node: PNode);
begin
   if Node <> nil then
   begin
      UnvisitAll(Node.Left);
      Node.Visited := 0;
      UnvisitAll(Node.Right);
   end;
end;

procedure TBST.SymmetricRightThread;
var
   Temp: PNode;
begin
   Prev := Root;

   // thread 'the most right element' to root

   Temp := Root;
   while Temp.Right <> nil do
      Temp := Temp.Right;

   Temp.RightThread := Root;

   TraverseThread(Root);
end;

procedure TBST.ThreeTraversals;
begin
   Writeln('-------------- RAB --------------');
   RAB(Root);
   Writeln;
   Writeln('---------------------------------');
   Writeln('-------------- ARB --------------');
   ARB(Root);
   Writeln;
   Writeln('---------------------------------');
   Writeln('-------------- ABR --------------');
   ABR(Root);
   Writeln;
   Writeln('---------------------------------');
end;

procedure TBST.Traverse;
begin
   Writeln('---------------------------------');
   Writeln;
   if Root = nil then
      Writeln('--- empty tree ---')
   else
      DFS(Root, '');
   Writeln;
   Writeln('---------------------------------');
end;


end.
